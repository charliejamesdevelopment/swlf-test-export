var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('register_step_1', {
    title: 'Register',
    user: (req.session.passport ? req.session.passport.user : undefined)
  });
});

module.exports = router;
