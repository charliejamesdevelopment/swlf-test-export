var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var user = new Schema({
  oauthID: Number,
  name: String,
  bio: { type: String, default: "This user has not yet edited their bio! Probably a noob?" },
  gamertags: {
    xbox: { type: String, default: "N/A" },
    psn: { type: String, default: "N/A" },
    origin: { type: String, default: "N/A" }
  },
  levels: {
    xbox: { type: Number, default: "1" },
    psn: { type: Number, default: "1" },
    origin: { type: Number, default: "1" }
  },
  created_at: Date,
  updated_at: Date
});

user.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at)
    this.created_at = currentDate;

  next();
});
user.set('collection', 'users');

var User = mongoose.model('User', user);

// make this available to our users in our Node applications
module.exports = User;
