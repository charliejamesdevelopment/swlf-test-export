module.exports = function(req){

  var clients = req.app.locals.clients
  var activities = req.app.locals.activities
  var language = req.app.locals.language
  var experience = req.app.locals.experience

  return {
    checkInArray: function(array, a) {
      if (array.indexOf(a) > -1) {
        return true;
      } else {
        return false;
      }
    },
    validate_post: function(body, fn) {
      var utils = require('./validation_utils')(req)
      if(utils.checkInArray(clients, body.client) &&
        utils.checkInArray(experience, body.experience) &&
        utils.checkInArray(language, body.language) &&
        utils.checkInArray(activities, body.gamemode)) {
          fn(null)
        } else {
          fn("Invalid post")
        }
    }
  }

}
