var mongoose = require('mongoose');
var User = require('../models/user');
var passport = require('passport');
var config = require('../../oauth.js');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
var jwt = require('jwt-simple')

function genToken(user) {
  var expires = expiresIn(7); // 7 days
  var token = jwt.encode({
    user: user,
    exp: expires
  }, require('../../database').secret);

  return {
    token: token,
    expires: expires,
    user: user
  };
}

function expiresIn(numDays) {
  var dateObj = new Date();
  return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = {
  configure: function() {
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    passport.deserializeUser(function(user, done) {
        done(null, user)
    });
    passport.use(new GoogleStrategy({
      clientID: config.google.clientID,
      clientSecret: config.google.clientSecret,
      callbackURL: config.google.callbackURL,
      passReqToCallback: true
      }, function(request, accessToken, refreshToken, profile, done) {
        process.nextTick(function() {
          token = genToken(profile.id)
          request.session.token = token;
          User.findOne({oauthID: profile.id}, function(err, user) {
            if(err) {
              return done(err, user);
            }
            if(!err && user) {
              return done(null, user);
            } else {
              var newUser = User({
                oauthID: profile.id,
                name: profile.displayName
              })
              newUser.save(function(err) {
                if(err) {
                  console.log(err);  // handle errors!
                } else {
                  console.log("saving user ...");
                  done(null, user);
                }
              });
            }
          });
          return done(null, profile);
        });
      }
    ));
  }

}
